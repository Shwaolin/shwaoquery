function ShwaoQuery() {

	// --- GLOBALS -----------------------------------------------------------------------------------------------------

	var self = this,
		$$els;

	// --- CONSTRUCTOR -------------------------------------------------------------------------------------------------

	if (arguments[0] instanceof ShwaoQuery) {

		return arguments[0];

	}
	else if (typeof arguments[0] === 'string') {

		var string = arguments[0].trim();

		if (string[0] === '<') {

			return $((new DOMParser()).parseFromString(string, 'text/html').body.childNodes);

		}

		$$els = Array.from(document.querySelectorAll(string));

	}
	else if (typeof arguments[0] === 'function') {

		$(document).on('DOMContentLoaded', arguments[0]);

	}
	else if (arguments[0] instanceof Window || arguments[0] instanceof HTMLDocument || arguments[0] instanceof Element) {

		$$els = [arguments[0]];

	}
	else if (arguments[0] instanceof Array || arguments[0] instanceof NodeList || arguments[0] instanceof HTMLCollection || arguments[0] instanceof HTMLAllCollection) {

		$$els = Array.from(arguments[0]);

	}

	// --- EXPOSE AS ARRAY In CONSOLE ----------------------------------------------------------------------------------

	if ($$els) {

		$$els.forEach(function($el, i) {

			self[i] = $el;

		});

		this.length = $$els.length;

	}

}

ShwaoQuery.prototype = {

	slice: Array.prototype.slice,

	splice: Array.prototype.splice,

	map: Array.prototype.map,

	forEach: Array.prototype.forEach,

	find: function() {

		var self = this,
			args = Array.from(arguments),
			$$newEls = [];

		self.forEach(function($el) {

			Element.prototype.querySelectorAll.apply($el, args).forEach(function($newEl) {

				$$newEls[$$newEls.length] = $newEl;

			});

		});

		return $($$newEls);

	},

	on: function() {

		var self = this,
			args = Array.from(arguments);

		self.forEach(function($el) {

			Element.prototype.addEventListener.apply($el, args);

		});

	},

	up: function(selector) {

		var self = this;

		return $(

			self.map(function($el) {

				var $parent = $el;

				do {

					$parent = $parent.parentElement;

				} while(selector && !$parent.matches(selector))

				return $parent;

			})

		);

	},

	appendTo: function() {

		var $appendTo = $(arguments[0]);

		this.forEach(function($el) {

			$appendTo[0].appendChild($el);

		});

	}

};

var $ = (function() {

	function F(args) {

		return ShwaoQuery.apply(this, args);

	}

	F.prototype = ShwaoQuery.prototype;

	return function() {

		return new F(arguments);

	};

})();
